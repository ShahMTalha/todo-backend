FROM ubuntu:latest
RUN apt-get update && apt-get install python3-pip python3-dev -y
RUN apt-get install libpq-dev -y
WORKDIR /todo-backend
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY . .
EXPOSE 5000
CMD ["python3", "main.py"]