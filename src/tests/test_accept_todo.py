import pytest
from src.tests import base

@pytest.mark.acceptance
def test_add_api():
    add_response, add_response_body = base.get_api_response('todo/add-acceptance')
    assert add_response_body['message'], "New todo is added successfully"
    list_response, list_response_body = base.get_api_response('todo/listing')
    assert list_response_body['data'][0]['id'] == add_response_body['data']['id']
    assert list_response_body['data'][0]['content'] == add_response_body['data']['content']
    assert list_response_body['data'][0]['type'] == add_response_body['data']['type']

