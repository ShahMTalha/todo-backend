<div id="top"></div>



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/ShahMTalha/todo-backend.git">
    <img src="images/todo.jpg" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Todo-Backend README</h3>

  <p align="center">
    Todo Listing backend project!
    <br />
    <a href="https://gitlab.com/ShahMTalha/todo-backend.git"><strong>Explore the docs »</strong></a>
    <br />
    <br />
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

This project is generally implemented to add todo and maintain the list. The project has some following key points

Here's why:
* Project is developed on test driven approach.
* This project has implemented a GitLab CI/CD covering the whole cycle.
* The pipeline manages deployment through docker to AWS EC2 instances (test and production)

Of course, this project is starting line So I'll be adding more in the near future. You may also suggest changes by forking this repo and creating a pull request or opening an issue.

Use the `README.md` to get started.

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

This app is developed in highly usable backend language  listed below.

* [Python 3.8](https://www.python.org/)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

Before jumping into the project please ensure that python3.8 is installed on your machine plus a running database (postgres, mysqli). After that verify following prerequisites.
* Ubuntu
  ```sh
  sudo apt-get update -y
  pip3 install --upgrade pip
  sudo apt install docker.io
  sudo apt install git-all
  sudo apt install python3-venv
  ```

### Installation

So please ensure to every step to clone the project and run it on your local machine successfully

1. Clone the repo
   ```sh
   git clone https://gitlab.com/ShahMTalha/todo-backend.git
   ```
2. Create your project virtual environment and activate it 
   ```sh
   cd your_project_folder
   python3 -m venv my-project-env
   source my-project-env/bin/activate
   ```
3. Install the required packages to run the app
   ```python
   pip3 install -r requirements.txt
   ```
4. Create a .env file and add your database url (In my case it is postgresql)
   ```python
   export DATABASE_URL=postgresql://user:password@host:port/your_db_name
5. Run the migrations 
   ```python
   source .env && python3 manage.py db init
   source .env && python3 manage.py db migrate
   source .env && python3 manage.py db upgrade
6. Run the flask app 
   ```python
   source .env && python3 main.py
   or
   export FLASK_APP=main.py
   flask run
   ```
7. Test cases are implemented using pytest which are mostly used to test apis (function base appoarch provides a lot of functionality like markers, fixtures, build-in hooks etc and is extended version of unit test library with a lot of support available) the very first command below will run all the cases but in specific we can specify markers i.e. unit for unit test cases and acceptance for acceptance test cases.
   ```python
   pytest
   or
   pytest -v -m unit
   pytest -v -m acceptance
   ```

   Moreover user acceptance test cases are written in python with selenium web driver for better coverage
   ```ssh
   pip3 install selenium
   python3 src/tests/uat/user_acceptance.py
   ```

   Contract base test will run through postman so better install it by following command and then import contract pact from src/test/contract_test/todo-contract.json to run apis for test coverage
   ```ssh
   sudo snap install postman
   ```
8. After completing all above dockerize your application locally. Firstly need to install docker:
   ```ssh
   sudo apt install docker.io -y
   ```
   If you want to store images on docker hub then login to your account and then you need to push and pull images on your docker account as well.
   ```ssh
   sudo docker login -u username -p password
   ```
   Then Dockerfile is present in the repository will be used to generate new image with tag latest once complete then push the image.
   ```ssh
   docker build -t image_name:latest .
   docker image push image_name:latest
   ```
   If the container name already exist then you need to remove it first and remove previous images as well.
   ```ssh
   docker ps -a
   docker rm -f container_name
   docker images 
   docker rmi -f image_id
   ```
   Then at very last pull the latest image and run the docker container by specifying port currently using 5000 for backend.
   ```ssh
   docker pull image_name:latest 
   docker run -d -p 5000:5000 --name container_name image_name:latest
   ```
<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

So by performing the above you will be able to run this project into your own setup and can add or view the todos.

I have deployed this frontend app on AWS EC2 :

Test Server : 3.129.62.132
Production Server : 18.219.141.59

As of yet several apis are exposed from backend:

Add: todo/add
Listing : todo/listing

<p align="right">(<a href="#top">back to top</a>)</p>


## Contact

Shah Muhammad Talha Tahir - [@linkedin_profile](https://www.linkedin.com/in/shah-muhammad-talha-tahir-27604698/) - shahmuhammadtalhatahir@gmail.com

Project Link: [https://gitlab.com/ShahMTalha/todo-backend.git](https://gitlab.com/ShahMTalha/todo-backend.git)




<p align="right">(<a href="#top">back to top</a>)</p>

